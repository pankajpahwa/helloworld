Chicken Tikka is a chicken dish originating in the Punjab region of the Indian Subcontinent made by baking chicken which has been marinated in spices and yoghurt. It should not be confused with Chicken Tikka Masala (of which it is an ingredient). It is traditionally cooked on skewers in a tandoor and is usually boneless.

Ingredients
675 g (1.5 lb) skinless and boneless chicken breast diced into large or small cubes
1 tbs mustard seed oil (see Notes, tips and variations)
50 ml (2fl oz) milk
150 ml (5fl oz) natural yoghurt
2 tbs lemon juice
1 tbs tomato puree
2 tbs chopped fresh cilantro (coriander leaves)
4 large cloves of garlic, finely chopped
1-4 chopped fresh red chillis (depending on strength of the Chillis and the desired strength of the marinade)
2 tsps paprika
2 tsps garam masala
1 tsp cumin powder
1/2 tsp turmeric
1 tsp salt (use rock salt for best results)
Procedure
Add all of the ingredients except the chicken to a non-metallic mixing bowl and mix well.
Add the chicken and mix until fully coated.
Ideally, the chicken should now be left in a refrigerator to marinate for at least 24 hours. It may not be safe to leave chicken which has previously been frozen for longer than 24 hours, but fresh chicken should be left to marinate for 48 hours for best results. If you do not wish to leave the chicken to marinate, simply continue to the next step.
The chicken now needs to be cooked. For best results, place on skewers and cook in a tandoor or ceramic pot oven. The chicken can also be cooked on skewers under a medium grill for 5-8 minutes on each side or barbecued (cooking times depend on the temperature of the barbecue). Always ensure the chicken is cooked throughout before serving. It should come apart easily when pressed down with the side of a fork.
Serve with naan, with rice or use in a Chicken Tikka Masala.
Notes, tips and variations
Mustard seed oil is considered unfit for human consumption in many parts of the world and is therefore not readily available. A poor alternative can be made by crackling 1/2 tsp of mustard seeds in approximately 2 tbs of very hot vegetable or olive oil. Allow to cool then use in place of mustard seed oil.
It is possible to make a tikka with almost any meat, the most commonly used other than chicken being lamb and mutton.
Tikka can be served either as main meal or as an appetiser.
Powdered or dried ingredients can be substituted for fresh, but at the expense of taste.
Some people prefer to make tikka with meat on-the-bone for improved flavour.
Links